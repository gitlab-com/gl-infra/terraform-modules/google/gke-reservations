
## Addresses

output "registry_gke_address" {
  value       = google_compute_address.registry-gke.address
  description = "Address of registry-gke"
}

output "git_gke_address" {
  value       = google_compute_address.git-gke.address
  description = "Address of git-gke"
}

output "prometheus_gke_address" {
  value       = google_compute_address.prometheus-gke.address
  description = "Address of prometheus-gke"
}

output "thanos_query_gke_address" {
  value       = google_compute_address.thanos-query-gke.address
  description = "Address of thanos-query-gke"
}

output "ssh_gke_address" {
  value       = google_compute_address.ssh-gke.address
  description = "Address of ssh-gke"
}

output "prometheus_ingress_iap_address" {
  value       = google_compute_global_address.prometheus-ingress-iap.address
  description = "Address of prometheus-ingress-iap"
}

## Self links

output "registry_gke_self_link" {
  value       = google_compute_address.registry-gke.self_link
  description = "Self link of registry-gke"
}

output "git_gke_self_link" {
  value       = google_compute_address.git-gke.self_link
  description = "Self link of git-gke"
}

output "prometheus_gke_self_link" {
  value       = google_compute_address.prometheus-gke.self_link
  description = "Self link of prometheus-gke"
}

output "thanos_query_gke_self_link" {
  value       = google_compute_address.thanos-query-gke.self_link
  description = "Self link of thanos-query-gke"
}

output "ssh_gke_self_link" {
  value       = google_compute_address.ssh-gke.self_link
  description = "Self link of ssh-gke"
}

output "prometheus_ingress_iap_self_link" {
  value       = google_compute_global_address.prometheus-ingress-iap.self_link
  description = "Self link of prometheus-ingress-iap"
}

# GitLab.com gke-reservations Terraform Module

THIS MODULE IS DEPRECATED AND HAS BEEN MOVED TO https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/tree/master/modules/gke-reservations
## What is this?

This module is used on the [gitlab-com-infrastructure](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure)
terraform repository for GitLab.com

This is a generic module that is used for reserving IP addresses that are used outside of Terraform.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written. Reading [Terraform: Up and Running](https://www.amazon.com/Terraform-Running-Writing-Infrastructure-Code-ebook/dp/B06XKHGJHP) is also recommended.

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).

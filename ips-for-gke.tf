# Reserved IP address for container registry
locals {
  # For backwards compatibility with the gitlab-gke regional cluster
  gke_name  = var.name == "gitlab-gke" ? format("gke-%v", var.environment) : format("gke-%v-%v", var.environment, var.name)
  prom_name = var.name == "gitlab-gke" ? format("iap-%v", var.environment) : format("gke-%v-%v", var.environment, var.name)
}

resource "google_compute_address" "registry-gke" {
  name         = "registry-${local.gke_name}"
  description  = "registry-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for git service traffic
resource "google_compute_address" "git-gke" {
  name         = "git-${local.gke_name}"
  description  = "git-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for prometheus operator
resource "google_compute_address" "prometheus-gke" {
  name         = "prometheus-${local.gke_name}"
  description  = "prometheus-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for thanos-query
resource "google_compute_address" "thanos-query-gke" {
  name         = "thanos-query-${local.gke_name}"
  description  = "thanos-query-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for ssh
resource "google_compute_address" "ssh-gke" {
  name         = "ssh-${local.gke_name}"
  description  = "ssh-${local.gke_name}"
  subnetwork   = var.gke_subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for prometheus ingress with IAP
resource "google_compute_global_address" "prometheus-ingress-iap" {
  name        = "prometheus-ingress-${local.prom_name}"
  description = "prometheus-ingress-${local.prom_name}"
}

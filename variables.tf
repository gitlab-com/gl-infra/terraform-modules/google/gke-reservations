variable "environment" {
  type        = string
  description = "The environment name"
}

variable "name" {
  type        = string
  description = "Unique name in the project to apply to reserved ips"
}

variable "gke_subnetwork_self_link" {
  type        = string
  description = "Subnetwork name for reserving GKE IP addresses"
}
